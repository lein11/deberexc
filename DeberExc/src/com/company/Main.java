package com.company;

import com.sun.org.apache.xpath.internal.operations.Or;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Object obj = new Object();
        Fruit fru = new Fruit();
        Apple app = new Apple();
        Citrus cit = new Citrus();
        Orange ora= new Orange();
        Squeezable squ = null;

        String res = "";

        //Casteo Objeto
        res += "\nChecking Cast for Objeto: ";
        res += "Fruit: ";
        try {
            obj = (Fruit) fru;
            res += "OK ";
        }
        catch(ClassCastException x){
            res += "NO ";

        }

        res += "Apple: ";
        try {
            obj = (Apple) app;
            res += "OK ";
        }
        catch(ClassCastException x1){
            res += "NO ";

        }

        res += "Squeezable: ";
        try {
            obj = (Squeezable) squ;
            res += "OK ";
        }
        catch(ClassCastException x2){
            res += "NO ";

        }

        res += "Citrus: ";
        try {
            obj = (Citrus) cit;
            res += "OK ";
        }
        catch(ClassCastException x3){
            res += "NO ";

        }

        res += "Orange: ";
        try {
            obj = (Orange) ora;
            res += "OK ";
        }
        catch(ClassCastException x4){
            res += "NO ";

        }




        //Casteo Fruit
        res += "\nChecking Cast for Fruit: ";
        res += "Fruit: ";
        try {
            fru = (Fruit) fru;
            res += "OK ";
        }
        catch(ClassCastException x5){
            res += "NO ";

        }

        res += "Apple: ";
        try {
            fru = (Apple) app;
            res += "OK ";
        }
        catch(ClassCastException x6){
            res += "NO ";

        }

        res += "Squeezable: ";
        try {
            fru = (Fruit) squ;
            res += "OK ";
        }
        catch(ClassCastException x7){
            res += "NO ";

        }

        res += "Citrus: ";
        try {
            fru = (Citrus) cit;
            res += "OK ";
        }
        catch(ClassCastException x8){
            res += "NO ";

        }

        res += "Orange: ";
        try {
            fru = (Orange) ora;
            res += "OK ";
        }
        catch(ClassCastException x9){
            res += "NO ";

        }




        //Casteo Apple
        res += "\nChecking Cast for Apple: ";
        res += "Fruit: ";
        try {
            app = (Apple) fru;
            res += "OK ";
        }
        catch(ClassCastException x10){
            res += "NO ";

        }

        res += "Apple: ";
        try {
            app = (Apple) app;
            res += "OK ";
        }
        catch(ClassCastException x11){
            res += "NO ";

        }








        //Casteo Citrus
        res += "\nChecking Cast for Citrus: ";
        res += "Fruit: ";
        try {
            cit = (Citrus) fru;
            res += "OK ";
        }
        catch(ClassCastException x12){
            res += "NO ";

        }
        res += "Squeezable: ";
        try {
            cit = (Citrus) squ;
            res += "OK ";
        }
        catch(ClassCastException x13){
            res += "NO ";

        }

        res += "Citrus: ";
        try {
            cit = (Citrus) cit;
            res += "OK ";
        }
        catch(ClassCastException x14){
            res += "NO ";

        }

        res += "Orange: ";
        try {
            cit = (Citrus) ora;
            res += "OK ";
        }
        catch(ClassCastException x15){
            res += "NO ";

        }





        //Casteo Orange
        res += "\nChecking Cast for Orange: ";
        res += "Fruit: ";
        try {
            ora = (Orange) fru;
            res += "OK ";
        }
        catch(ClassCastException x16){
            res += "NO ";

        }

        res += "Squeezable: ";
        try {
            ora = (Orange) squ;
            res += "OK ";
        }
        catch(ClassCastException x17){
            res += "NO ";

        }

        res += "Citrus: ";
        try {
            ora = (Orange) cit;
            res += "OK ";
        }
        catch(ClassCastException x18){
            res += "NO ";

        }

        res += "Orange: ";
        try {
            ora = (Orange) ora;
            res += "OK ";
        }
        catch(ClassCastException x19){
            res += "NO ";

        }

        System.out.println(res);


    }
}
